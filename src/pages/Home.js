import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';

export default function Home() {
	return(
		<>
			<Banner />
			<Highlights />

		</>
	)
}