import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {

	// State hooks to store the values of our input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not

	const { user, setUser } = useContext(UserContext);

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);



	// useEffect() - whenever there is a a change in our webpage
	useEffect (() => {
		if((email !== '' && password1 !== '' && password2 !== '') && password1 === password2) {

			setIsActive(true)

		} else {

			setIsActive(false)

		}
	})

	function registerUser(e) {

		e.preventDefault();
		fetch('http://localhost: 4000/users/register', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password1: password1,
				password2: password2
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if (typeof data.access !== "undefined") {

          localStorage.push('token', data.access)


          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Zuitt!"
          })
        } else {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please provide a different email."
          })
        }

		})


		// clear input fields
		setEmail('');
		setPassword1('');
		setPassword2('');

		// alert('Thank you po for registering! ')
	}

	// onChange - checks if there are any changes inside of the input fields
	// value={email} - the value stored in the input field will come from the value inside the getter "email"
	// setEmail(e.target.value) - sets the value of the getter email to the value store d in the value={email} input field


	return(
		//(user.email !== null ) ?
      //<Navigate to="/login"/>
    //:
		 <Form onSubmit={e => registerUser(e)}>

		  <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control type="firstName" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control type="lastName" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
      </Form.Group>



      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
      </Form.Group>
    

      <Form.Group className="mb-3" controlId="formBasicPassword2 ">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
      </Form.Group>

      {/*Ternary Operator*/}

      {/*
			if (isActive === true) {
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			} else {
				<Button variant="danger" type="submit" id="submitBtn" disabled >Submit</Button>
			}

      */}
      
      {
      	isActive ?
      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
      	:
      		<Button variant="danger" type="submit" id="submitBtn" disabled >Submit</Button>
      }


      
    </Form>
	)
}