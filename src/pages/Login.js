import {Button, Form} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {

  // Allows us to consume the UserContext object and its properties for user validation
  const { user, setUser } = useContext(UserContext);

  

  // State Hooks to store the values of our input fields
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password);

  // useEffect() - whenever there is a change in our webpage
    useEffect (() => {
      if(email !== '' && password !== '') {

        setIsActive(true)

      } else {

        setIsActive(false)

      }
    })





  

  
function loginUser(e) {

      e.preventDefault();

      // Process a fetch request to the corresponding backend API
      fetch('http://localhost:4000/users/login', {
        method: "POST",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res => res.json())
      .then(data => {

        console.log(data)

        // If no user information is found, the "access" property will not be available and will return undefined
          // Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
        if (typeof data.access !== "undefined") {

          localStorage.setItem('token', data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Zuitt!"
          })
        } else {
          Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login credentials and try again later"
          })
        }

      })

      // Set the email of the authenticated user in the local storage
      // localStorage.setItem("email", email)

      // user = {email: localStorage.getItem('email')}
      // setUser({
      //  email: localStorage.getItem('email')
      // })

      // clear input fields
      setEmail('');
      setPassword('');

      // alert('Successful login');
    }

    const retrieveUserDetails = (token) => {

      // The token will be sent as part of the request's header information
      // We put "Bearer" in front of the token to follow implementation standards for JWTs
      fetch('http://localhost:4000/users/details', {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res => res.json())
      .then(data => {

        console.log(data);

        // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })

    }


  return (
    (user.id !== null ) ?
      <Navigate to="/courses"/>
    :
    <Form onSubmit={e => loginUser(e)}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
      </Form.Group>
      

      <Button variant="success" type="submit">
        Login
      </Button>
    </Form>
  );
} 

