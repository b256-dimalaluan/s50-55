import { Navigate } from 'react-router-dom';
import Banner from '../components/Banner.js';

import { Row, Col } from 'react-bootstrap';

export default function ErrorPage() {
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Page Not Found</h1>
				<p>Go back to the <a href="/" class="stretched-link">homepage.</a></p> 	

					
			</Col>
		</Row>
	)
}