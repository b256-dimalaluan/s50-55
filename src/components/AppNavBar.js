// React Bootstrap Components

import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../userContext';


export default function AppNavBar() {

	// Statehooks to store user information in the login page
	//const [user, setUser] = useState(localStorage.getItem("email"));
	//console.log(user);

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
      		<Container>
		        <Navbar.Brand as={Link} to="/">Zuitt Bootcamp</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
          			<Nav className="me-auto">
            			<Nav.Link as={NavLink} to="/">Home</Nav.Link>
            			<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
            			{
            				(user.id !== null) ?
            					<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            				:
            				<>
            					<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            					<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            				</>
            			}
            			
		            </Nav>
		        </Navbar.Collapse>
		     </Container>
	    </Navbar>
	)
}